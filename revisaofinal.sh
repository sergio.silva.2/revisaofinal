bash
#!/bin/bash

# Nome do arquivo
arquivo="revisaofinal.txt"
# URL para download do arquivo
url="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"

# Verifica se o arquivo existe na pasta atual, caso contrário, faz o download
if [ ! -f "$arquivo" ]; then
    echo "Fazendo o download do arquivo..."
    wget "$url"
fi

# Exibe os endereços IP únicos e ordenados do arquivo usando grep, expressões regulares e sort
ips=$(grep -E -o "([0-9]{1,3}\.){3}[0-9]{1,3}" "$arquivo" | sort -u)

echo "Endereços IP únicos e ordenados:"
echo "$ips"

# Usa o AWK para contar a ocorrência de cada IP
echo "Contagem de ocorrências de cada IP:"
awk '{count[$1]++} END {for (ip in count) print ip ":", count[ip]}' "$arquivo"

# Usa o sed para substituir cada endereço IP no arquivo pela palavra SUPRIMIDO
sed -i 's/\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}/SUPRIMIDO/g' "$arquivo"

# Usa o sed para substituir apenas o 1º e 4º octetos de cada endereço IP por XXX
sed -i 's/\([0-9]\{1,3\}\.\)[0-9]\{1,3\}\.\([0-9]\{1,3\}\.\)[0-9]\{1,3\}/XXX.\2XXX./g' "$arquivo"

echo "Processamento concluído."
